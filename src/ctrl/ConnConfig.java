package ctrl;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ResourceBundle;

/**
 * Created by Pouyan on 6/Dec/2017.
 */
public class ConnConfig {
    //<editor-fold desc="Variables">
    private ResourceBundle resourceBundle;
    private String db;
    private String dbDec;
    private String ip;
    private String port;
    private String DBname;
    private String username;
    private String password;
    private String url;
    private Connection connection;
    //</editor-fold>

    /**
     * Reads the property Strings from <code>config.properties</code>
     * and sets the variables to generate a connection url.
     */
    public void setBundle() throws java.util.MissingResourceException {
        this.resourceBundle = ResourceBundle.getBundle("setting.config");
        this.db = resourceBundle.getString("db");
        this.dbDec = "jdbc:" + this.db + ":thin://";
        this.ip = resourceBundle.getString("ip");
        this.port = resourceBundle.getString("port");
        this.DBname = resourceBundle.getString("DB_NAME");
        this.username = resourceBundle.getString("username");
        this.password = resourceBundle.getString("password");
        this.url = dbDec + "@" + ip + ":" + port + ":" + DBname;
        //resourceBundle
    }

    /**
     * Sets the value of a <code>java.sql.Connection connection</code>
     * from <code>DriverManager</code> using variables of this class.
     *
     * <B>Note:</B> <code>setBundle</code> or setter functions must be
     * used before calling this function.
     * <p></p>
     * <B>Note:</B> the opened connection must be somehow closed later.
     * @return established <code>this.connection</code>
     * @throws SQLException if connection could not be established.
     */
    public Connection setupConn() throws SQLException {
        this.connection = DriverManager.getConnection(this.url, this.username, this.password);
        return this.connection;
    }

    /**
     * A morph of <code>setupConn()</code> but uses variables from entry
     * and has nothing to do with private variables of this class.
     * @param url used as Connection URL
     * @param username used as the username of the database
     * @param password used as the password of the database
     * @return established connection in case that there was no exception.
     * @throws SQLException if connection could not be established
     */
    public static Connection setupConn(String url, String username, String password)
    throws SQLException {
        return DriverManager.getConnection(url, username, password);
    }

    /**
     * Getters and setters in order to increase
     * the efficiency and usability of the class
     * for external usages.
     * <p></p>
     * <B>Note:</B> Using this method of setting
     * variables does NOT grantee a perfect
     * tasking for the class. Use with caution.
     *
     * @return returns private object.
     */
    //<editor-fold desc="Getters and Setters">
    public ResourceBundle getResourceBundle() {
        return resourceBundle;
    }

    public void setResourceBundle(ResourceBundle resourceBundle) {
        this.resourceBundle = resourceBundle;
    }

    public String getDb() {
        return db;
    }

    public void setDb(String db) {
        this.db = db;
    }

    public String getDbDec() {
        return dbDec;
    }

    public void setDbDec(String dbDec) {
        this.dbDec = dbDec;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }

    public String getDBname() {
        return DBname;
    }

    public void setDBname(String DBname) {
        this.DBname = DBname;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Connection getConnection() {
        return connection;
    }

    public void setConnection(Connection connection) {
        this.connection = connection;
    }
    //</editor-fold>
}