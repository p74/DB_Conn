package ctrl;

/**
 * Created by Pouyan on 9/Dec/2017.
 */
public class ConnJDBC {
    private ConnConfig connConfig;
    private java.sql.Statement statement;
    private String query;

    /**
     * Primary constructor which sets a <code>ConnConfig</code>
     * object right away.
     * @param connConfig
     */
    public ConnJDBC(ConnConfig connConfig) throws java.sql.SQLException {
        this.connConfig = connConfig;
        this.statement = this.connConfig.getConnection().createStatement();
    }

    /**
     * Executes the private query in this class using
     * it's statement. Simple!!
     * @return if the execution worked or not.
     * @throws java.sql.SQLException
     */
    public boolean executeQuery() throws java.sql.SQLException {
        return this.statement.execute(this.query);
    }

    /**
     * Executes the private query if it's an update one
     * using the statement.
     * @return executeUpdate code.
     * @throws java.sql.SQLException
     */
    public int executeUpdateQuery() throws java.sql.SQLException {
        return this.statement.executeUpdate(this.query);
    }
}
