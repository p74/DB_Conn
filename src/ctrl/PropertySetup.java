package ctrl;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;

/**
 * Created by Pouyan on 9/Dec/2017.
 */

public class PropertySetup {
    public static final String CONFIG_FILE = "setting.config";
    private static PropertiesConfiguration propCon;

    /**
     * Come back here ASAP
     * @param db
     * @throws ConfigurationException
     */
    public static void setDB(String db) throws ConfigurationException {
        propCon = new PropertiesConfiguration(CONFIG_FILE);
    }
}
